**Application to train ReactJS**
---

# Library used for this project :
1. **apisauce** -> *https://github.com/infinitered/apisauce*
2. **firebase** -> *https://firebase.google.com/*
3. **formik** -> *https://github.com/jaredpalmer/formik*
4. **prop-types** -> *https://www.npmjs.com/package/prop-types*
5. **react-bootstrap** -> *https://react-bootstrap.github.io/*
6. **react-dom** -> *https://reactjs.org/docs/react-dom.html*
7. **react-redux** -> *https://redux.js.org/basics/usage-with-react*
8. **react-router-dom** -> *https://www.npmjs.com/package/react-router-dom*
9. **recompose** -> *https://github.com/acdlite/recompose*
10. **redux** -> *https://redux.js.org/basics/usage-with-react*
11. **reduxsauce** -> *https://github.com/infinitered/reduxsauce*
12. **redux-persist** -> *https://github.com/rt2zz/redux-persist*
13. **redux-saga** -> *https://github.com/redux-saga/redux-saga*
14. **redux-saga-firebase** -> *https://www.npmjs.com/package/redux-saga-firebase*
15. **yup** -> *https://github.com/jquense/yup*

---

# Final goals : 

## At the beginning
1. a simple react example app:
  + log in screen
  + home screen with log out button
2. login function check username and password then make request to httpbin for mocking real server request
3. then set authentication in redux when receiving response
4. logout funciton similar to login and clear authentication in redux
5. This example will use: redux, redux saga, react router, reduxsauce, apisauce

## At the end
1. SignUp Page -> Register with Firestore
2. Login Page -> Authentification with Firestore
3. Home Page -> Side bar to navigate
### Home Page :
1. Dashboard
2. Multiple Forms for a user -> Datas saved into Firestore's database
3. Search data by client or project -> Query on Firestore's database
4. Show all the data of a user -> Read from Firestore