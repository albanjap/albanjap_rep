// index.js from firebase
// Import
import { firebaseApp as firebase } from './rsf'

// Export
export {
  firebase
}
