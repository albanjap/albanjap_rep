// rsf.js
// Imports
import * as firebase from 'firebase'
import ReduxSagaFirebase from 'redux-saga-firebase'

// Initialize Firebase
const firebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyAhCZxMsXj4ql7JJS4KtY6K9vRy3EzraK4',
  authDomain: 'randomproject-6cdb4.firebaseapp.com',
  databaseURL: 'https://randomproject-6cdb4.firebaseio.com',
  projectId: 'randomproject-6cdb4',
  storageBucket: 'randomproject-6cdb4.appspot.com',
  messagingSenderId: '359093121525'
})

const rsf = new ReduxSagaFirebase(firebaseApp)

// Exports
export { firebaseApp }

export default rsf
