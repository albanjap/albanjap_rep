// Actions.js

// Imports
import { createActions } from 'reduxsauce'

// Actions
const { Types, Creators } = createActions({

  // login
  login: ['data'],
  loginSuccess: ['userData', 'counter'],
  loginFailure: ['error'],

  // register
  registerWithEmail: ['data'],
  registerWithEmailSuccess: ['credential'],
  registerWithEmailFailure: ['error'],

  // logout
  logout: null,
  logoutSuccess: null,
  logoutFailure: ['error'],

  // save data
  saveUserData: ['userData'],
  saveUserDataSuccess: ['userData'],
  saveUserDataFailure: ['error'],

  // reset data
  resetUserData: ['payload'],
  resetUserDataSuccess: ['userData'],
  resetUserDataFailure: ['error'],

  // get selected Tab
  selectedTab: ['key'],

  // search data
  searchData: ['searchData', 'client', 'project'],

  // change page for pagination
  changePage: ['page']

})

const logActions = {
  Types,
  Creators
}

// export
export default logActions
