// DataForm.js

// Imports
import React, { Component } from 'react'
import { Form, Button, InputGroup } from 'react-bootstrap'

import withDataForm from '../../enhancers/withDataForm'

// DataForm class
class DataForm extends Component {
  render () {
    // variables
    const { touched, errors, handleChange, handleSubmit, setFieldValue, values } = this.props
    return (
      <Form noValidate onSubmit={handleSubmit}>
        {/* Email address */}
        <Form.Group>
          <Form.Label>Email address</Form.Label>

          <Form.Control type='email'
            name='email'
            placeholder='address@mail.com'
            value={values.email}
            onChange={handleChange}
            isInvalid={touched.email && errors.email}>
          </Form.Control>

          <Form.Control.Feedback type='invalid'>{errors.email}</Form.Control.Feedback>
        </Form.Group>

        {/* Phone Number */}
        <Form.Group>
          <Form.Label>Phone number</Form.Label>
          <InputGroup>

            <InputGroup.Prepend>
              <InputGroup.Text id='inputGroupPrepend'>+84</InputGroup.Text>
            </InputGroup.Prepend>

            <Form.Control type='phone'
              name='phoneNumber'
              value={values.phone}
              onChange={handleChange}
              isInvalid={touched.phone && errors.phone}>
            </Form.Control>

            <Form.Control.Feedback type='invalid'>{errors.phone}</Form.Control.Feedback>
          </InputGroup>
        </Form.Group>

        {/* Gender */}
        <Form.Group>
          <Form.Check inline label='Male'
            type='radio'
            name='gender'
            value='Male'
            onChange={handleChange}
            isInvalid={touched.gender && errors.gender}>
          </Form.Check>

          <Form.Check inline label='Female'
            type='radio'
            name='gender'
            value='Female'
            onChange={handleChange}
            isInvalid={touched.gender && errors.gender}>
          </Form.Check>

          <Form.Check inline label='Other'
            type='radio'
            name='gender'
            value='Other'
            onChange={handleChange}
            isInvalid={touched.gender && errors.gender}>
          </Form.Check>
        </Form.Group>

        {/* City */}
        <Form.Group>
          <Form.Label>Choose a city</Form.Label>

          <Form.Control as='select'
            name='city'
            value={values.city}
            onChange={handleChange}
            isInvalid={touched.city && errors.city} >
            <option value=''> Select a city</option>
            <option value='Hanoi'>Hanoi</option>
            <option value='Paris'>Paris</option>
            <option value='New York'>New York</option>
          </Form.Control>

          <Form.Control.Feedback type='invalid'>{errors.city}</Form.Control.Feedback>
        </Form.Group>

        {/* Client */}
        <Form.Group>
          <Form.Label>Choose a client</Form.Label>

          <Form.Control as='select'
            name='client'
            value={values.client}
            onChange={handleChange}
            isInvalid={touched.client && errors.client} >
            <option value=''> Select a client</option>
            <option value='Gilbert Montagné'>Gilbert Montagné</option>
            <option value='Claude François'>Claude François</option>
            <option value='Véronique Sanson'>Véronique Sanson</option>
          </Form.Control>

          <Form.Control.Feedback type='invalid'>{errors.client}</Form.Control.Feedback>
        </Form.Group>

        {/* If one client was selected, then Project */}
        {values.client ? <Form.Group>
          <Form.Label>Choose a project: </Form.Label>

          <Form.Control as='select'
            name='project'
            value={values.project}
            onChange={handleChange}
            isInvalid={touched.project && errors.project} >
            <option value=''>Select a project</option>
            <option value='Jouer du piano'>Jouer du piano</option>
            <option value='Changer une ampoule'>Changer une ampoule</option>
            <option value='Boire et fumer'>Boire et fumer</option>
          </Form.Control>

          <Form.Control.Feedback type='invalid'>{errors.project}</Form.Control.Feedback>
        </Form.Group>
          : null
        }

        {/* Image */}
        <div className='form-group'>
          <label>Upload image</label>
          <input id='file'
            name='file'
            type='file'
            onChange={(event) => {
              setFieldValue('image', event.currentTarget.files[0])
            }} />
        </div>

        <Button type='submit'>Submit form</Button>

      </Form>
    )
  }
}

// export
export default withDataForm(DataForm)
