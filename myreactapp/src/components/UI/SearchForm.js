// SearchForm.js

// Imports
import React, { Component } from 'react'
import { Form, Button } from 'react-bootstrap'

import withSearchForm from '../../enhancers/withSearchForm'

// SearchForm
class SearchForm extends Component {
  render () {
    // variables
    const { touched, errors, handleChange, handleSearch, values } = this.props
    return (
      <Form noValidate onSubmit={handleSearch}>
        {/* Search */}
        <Form.Group>
          <Form.Label>Search</Form.Label>

          <Form.Control name='search'
            type='text'
            value={values.search}
            onChange={handleChange}
            isInvalid={touched.search && errors.search}>
          </Form.Control>

          <Form.Control.Feedback type='invalid'>{errors.search}</Form.Control.Feedback>
        </Form.Group>

        {/* Filter */}
        <Form.Group>
          <Form.Check inline label='Client'
            type='checkbox'
            name='client'
            value='Client'
            onChange={handleChange}
            isInvalid={touched.client && errors.client}>
          </Form.Check>

          <Form.Check inline label='Project'
            type='checkbox'
            name='project'
            value='Project'
            onChange={handleChange}
            isInvalid={touched.project && errors.project}>
          </Form.Check>
        </Form.Group>

        <Button type='submit'>Search</Button>
      </Form>
    )
  }
}

export default withSearchForm(SearchForm)
