// DashboardView.js

// Imports
import React, { Component } from 'react'
import { Row, Nav, Tab, Col, Button, Pagination } from 'react-bootstrap'
import { Redirect } from 'react-router-dom'

import withDashboard from '../../enhancers/withDashboard'
import DataForm from '../UI/DataForm'
import SearchForm from '../UI/SearchForm'

// DashboardView
class Dashboard extends Component {
  // Pagination function
  pagination () {
    // variables
    const { nbPages } = this.props
    var index = []
    var i

    // Paginate all the pages
    for (i = 0; i < nbPages; i++) {
      index.push(i)
    }
    return index
  }

  // Loading all the data on the page
  dataPage () {
    // Variables
    const { activePage, searchedData, userData, clientPerPage } = this.props
    var response = []
    var i
    var data = searchedData || userData

    // Checking all the data
    for (i = (activePage - 1) * clientPerPage; i < ((activePage - 1) * clientPerPage) + clientPerPage; i++) {
      if (data[i]) {
        response[i] = data[i]
      }
    }
    return response
  }

  render () {
    // Variables
    const { loggedIn, handleLogout, userData, handleResetData, handleSelect,
      /* searchString, searchedData, nb, */ nbPages, handleChangePage, activePage } = this.props

    // If not loggedIn, redirect to the login page
    if (!loggedIn) {
      return <Redirect to='/login' />
    }

    return (
      <div id='Dashboard'>
        <Tab.Container onSelect={handleSelect} defaultActiveKey='home'>
          <Row noGutters>
            {/* SideBar menu */}
            <Col sm={2} className='text-center bg-light'>
              <Nav className='flex-column' variant='pills'>

                {/* Dasboard */}
                <Nav.Item>
                  <Nav.Link eventKey='home'>Dasboard</Nav.Link>
                </Nav.Item>

                {/* Form */}
                <Nav.Item>
                  <Nav.Link eventKey='form'>Form</Nav.Link>
                </Nav.Item>

                {/* Search */}
                <Nav.Item>
                  <Nav.Link eventKey='search'>Search</Nav.Link>
                </Nav.Item>

                {/* If datas for the user has been found, show them */}
                {userData ? userData.map((data, index) => {
                  if (data) {
                    return (
                      // User Data
                      <Nav.Item key={index}>
                        <Nav.Link eventKey={data.id}>{data.client}</Nav.Link>
                      </Nav.Item>
                    )
                  }
                  return null
                })
                  : null}

                {/* Logout */}
                <Nav.Item>
                  <Nav.Link href='' onClick={handleLogout}>Logout</Nav.Link>
                </Nav.Item>

              </Nav>
            </Col>
            <Col sm={1} />

            <Col sm={8} className='text-center'>

              <Tab.Content>

                {/* Home tab */}
                <Tab.Pane eventKey='home'>
                  <div>
                    <h1>Welcome user!</h1>
                        You're logged in!
                    <br />
                  </div>
                </Tab.Pane>

                {/* Form tab */}
                <Tab.Pane eventKey='form'>
                  <div id='Form'>
                    {/* Call the DataForm in UI */}
                    <DataForm />
                  </div>
                </Tab.Pane>

                {/* Search tab */}
                <Tab.Pane eventKey='search'>
                  {/* Call the SearchForm in UI */}
                  <SearchForm />
                  <br />
                  <br />
                  {/* If data has been found */}
                  {this.dataPage().map((data, index) => {
                    return (
                      <div key={index}>
                        <Row>
                          {/* Image */}
                          <Col>
                            {data.image
                              ? <img src={data.image}
                                width={100}
                                alt='' />
                              : <p>Image not found</p>}
                          </Col>

                          {/* Client and project names */}
                          <Col>
                            <p>{data.client}</p>
                            <p>{data.project}</p>
                          </Col>

                          <Col>
                            <div>
                              <Button onClick={() => handleSelect(data.id)}>
                                Details
                              </Button>
                            </div>
                          </Col>
                        </Row>
                        <br />
                      </div>
                    )
                  })}

                  {/* Pagination */}
                  {nbPages
                    ? <Pagination className='text-center'>
                      {this.pagination().map((data, index) => {
                        return (
                          <Pagination.Item id={index + 1}
                            key={index + 1}
                            onClick={handleChangePage}
                            active={(index + 1) === activePage}>
                            {index + 1}
                          </Pagination.Item>
                        )
                      })}
                    </Pagination>
                    : null
                  }
                </Tab.Pane>

                {/* User datas */}
                {userData
                  ? userData.map((data, index) => {
                    if (data) {
                      return (
                        <Tab.Pane eventKey={data.id} key={index}>
                          <br />
                          <p>Your email is : {data.email}</p>
                          <p>Your phone nb is : {data.phonenb}</p>
                          <p>Your gender is : {data.gender}</p>
                          <p>You live in {data.city}</p>
                          <p>Your client is : {data.client}</p>
                          <p>Your project is : {data.project}</p>
                          {data.image
                            ? <img src={data.image}
                              alt=''
                              width={200} />
                            : <p>Image not uploaded</p>
                          }
                          <br />
                          <Button type='submit' onClick={handleResetData}>Delete</Button>
                        </Tab.Pane>
                      )
                    }
                    return null
                  })
                  : null
                }
              </Tab.Content>
            </Col>
          </Row>
        </Tab.Container>
      </div>
    )
  }
}

// export
export default withDashboard(Dashboard)
