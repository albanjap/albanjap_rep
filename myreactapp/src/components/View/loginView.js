// loginView.js

// Imports
import React from 'react'
import { Form, Col, Button, Row } from 'react-bootstrap'
import { Redirect, Link } from 'react-router-dom'

import withLogin from '../../enhancers/withLogin'

// Export
export default withLogin(({ loggedIn, handleSubmit, handleChange, touched, errors }) => {
  // If you're logged in, redirect to the dasboard page
  if (loggedIn) {
    return <Redirect to='/dashboard' />
  }

  // Form to login
  return (
    <div id='Login'>
      <Row className='justify-content-md-center'>
        {/* >When you click on the submit button, it calls onHandleLogin */}
        <Col md='auto' className='text-center'>
          <h1>Welcome! Please sign in :</h1>

          <Form noValidate onSubmit={handleSubmit}>

            {/* Email address input */}
            <Form.Group controlId='validationUser' >
              <Form.Label>Email address</Form.Label>

              <Form.Control
                type='text'
                name='email'
                placeholder='address@mail.com'
                onChange={handleChange}
                isInvalid={touched.email && errors.email}
              />

              <Form.Control.Feedback type='invalid'>{errors.user}</Form.Control.Feedback>
            </Form.Group>

            {/* Password input */}
            <Form.Group controlId='validationPsswd'>
              <Form.Label>Password</Form.Label>

              <Form.Control
                type='password'
                name='password'
                placeholder='password'
                onChange={handleChange}
                isInvalid={touched.password && errors.password}
              />

              <Form.Control.Feedback type='invalid'>{errors.password}</Form.Control.Feedback>
            </Form.Group>

            <Button type='submit'>Login</Button>
            <p>
              Don't have an account? <Link to='/signUp'>Sign Up</Link>
            </p>
          </Form>
        </Col>
      </Row>
    </div>
  )
})
