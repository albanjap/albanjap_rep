// signUpView.js

// Imports
import React from 'react'
import { Form, Button } from 'react-bootstrap'
import { Link, Redirect } from 'react-router-dom'

import withSignUp from '../../enhancers/withSignUp'

// Export
export default withSignUp(({ loggedIn, handleSubmit, handleChange, touched, errors }) => {
  // If you're logged in, redirect to the dasboard page
  if (loggedIn) {
    return <Redirect to='/dashboard' />
  }

  // Form to Sign Up
  return (
    <Form noValidate onSubmit={handleSubmit}>

      {/* Username input */}
      <Form.Group controlId='validationUser' >
        <Form.Label>Username</Form.Label>

        <Form.Control
          type='text'
          name='user'
          placeholder='username'
          onChange={handleChange}
          isInvalid={touched.user && errors.user}
        />

        <Form.Control.Feedback type='invalid'>{errors.user}</Form.Control.Feedback>
      </Form.Group>

      {/* Email input */}
      <Form.Group controlId='validationEmail' >
        <Form.Label>Email</Form.Label>

        <Form.Control
          type='text'
          name='email'
          placeholder='email'
          onChange={handleChange}
          isInvalid={touched.email && errors.email}
        />

        <Form.Control.Feedback type='invalid'>{errors.email}</Form.Control.Feedback>
      </Form.Group>

      {/* Password input */}
      <Form.Group controlId='validationPsswd'>
        <Form.Label>Password</Form.Label>

        <Form.Control
          type='password'
          name='password'
          placeholder='password'
          onChange={handleChange}
          isInvalid={touched.password && errors.password}
        />

        <Form.Control.Feedback type='invalid'>{errors.password}</Form.Control.Feedback>
      </Form.Group>

      {/* Confirm Password input */}
      <Form.Group controlId='validationConfirmPsswd'>
        <Form.Label>Confirmation Password</Form.Label>

        <Form.Control
          type='password'
          name='confirmPassword'
          placeholder='Confirm password'
          onChange={handleChange}
          isInvalid={touched.confirmPassword && errors.confirmPassword}
        />

        <Form.Control.Feedback type='invalid'>{errors.confirmPassword}</Form.Control.Feedback>
      </Form.Group>

      <Button type='submit'>Sign Up</Button>
      <p>
        You already have an account? <Link to='/login'>Sign In</Link>
      </p>
    </Form>
  )
}
)
