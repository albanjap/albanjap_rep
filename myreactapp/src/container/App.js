// App.js

// Imports
import PropTypes from 'prop-types'
import React, { Component } from 'react'

class App extends Component {
  render () {
    return (
      <div>
        {this.props.children}
      </div>
    )
  }
}

App.propTypes = {
  children: PropTypes.object.isRequired
}

// Export
export default App
