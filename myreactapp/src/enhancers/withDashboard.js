// withDashboard.js

// Imports
import { connect } from 'react-redux'
import { withHandlers, compose } from 'recompose'

import Actions from '../actions/Actions'

const mapStateToProps = state => (
  {
    ...state.auth
  }
)

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch(Actions.Creators.logout()),
  resetUserData: (form) => dispatch(Actions.Creators.resetUserData(form)),
  handleSelect: (key) => dispatch(Actions.Creators.selectedTab(key)),
  changePage: (page) => dispatch(Actions.Creators.changePage(page))
})

// HANDLER
const handler = withHandlers({

  // Logout
  handleLogout: ({ logout }) => event => {
    event.preventDefault()
    logout()
  },

  // Reset Datas
  handleResetData: ({ resetUserData, form }) => event => {
    event.preventDefault()
    resetUserData(form)
  },

  // Change Page
  handleChangePage: ({ changePage }) => event => {
    event.preventDefault()
    changePage(event.target.id)
  }
})

// Export
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  handler
)
