// withDataForm.js

// Imports
import { withFormik } from 'formik'
import { connect } from 'react-redux'
import { compose } from 'recompose'
import * as Yup from 'yup'

import Actions from '../actions/Actions'

const mapStateToProps = state => (
  {
    ...state.auth
  }
)

const mapDispatchToProps = dispatch => ({
  saveUserData: (userData, i) => dispatch(Actions.Creators.saveUserData(userData, i))
})

// FORM
const form = withFormik({
  initialValues: {
    email: '',
    phoneNumber: '',
    gender: '',
    city: '',
    client: '',
    project: '',
    image: ''
  },

  mapPropsToValues: () => ({
    email: '',
    phoneNumber: '',
    gender: '',
    city: '',
    client: '',
    project: '',
    image: ''
  }),

  validationSchema: () => {
    return Yup.object().shape({
      email: Yup.string().email('Need to be a valid email address').required('Email required'),
      phoneNumber: Yup.string().length(10, 'Need to be a valid phone number').required('Phone number required'),
      gender: Yup.string().required('Gender required'),
      city: Yup.string().required('City required'),
      client: Yup.string().required('Client required'),
      project: Yup.string().required('Project required')
    })
  },

  // SUBMIT
  handleSubmit: (values, actions) => {
    // Variables
    let email = values.email
    let phoneNumber = values.phoneNumber
    let gender = values.gender
    let city = values.city
    let client = values.client
    let project = values.project
    let image = values.image ? values.image : null

    const data = {
      email,
      phoneNumber,
      gender,
      city,
      client,
      project,
      image
    }

    // Action call
    actions.props.saveUserData(data)
    actions.setSubmitting(false)
  }
})

// Export
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  form
)
