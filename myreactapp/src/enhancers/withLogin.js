// withLogin.js

// Imports
import { withFormik } from 'formik'
import { connect } from 'react-redux'
import { compose } from 'recompose'

import logActions from '../actions/Actions'

const mapStateToProps = state => (
  {
    ...state.auth
  }
)

const mapDispatchToProps = dispatch => ({
  loginSaga: data => dispatch(logActions.Creators.login(data))
})

// FORM
const form = withFormik({
  initialValues: {
    email: '',
    password: ''
  },

  mapPropsToValues: () => ({
    email: '',
    password: ''
  }),

  // SUBMIT
  handleSubmit: (values, actions) => {
    // Variables
    let email = values.email
    let password = values.password

    const data = {
      email,
      password
    }

    // Action call
    actions.props.loginSaga(data)
    actions.setSubmitting(false)
  },

  // Check errors
  validate: (values) => {
    const errors = {}
    if (!values.email) errors.email = 'Enter an email'
    if (!values.password) errors.password = 'Enter a password'
    return errors
  }
}

)

// Export
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  form
)
