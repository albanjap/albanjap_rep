// withSearchForm.js

// Import
import { withFormik } from 'formik'
import { connect } from 'react-redux'
import { compose } from 'recompose'

import Actions from '../actions/Actions'

const mapStateToProps = state => (
  {
    ...state.login
  }
)

const mapDispatchToProps = dispatch => ({
  saveUserData: (userData) => dispatch(Actions.Creators.saveUserData(userData)),
  searchData: (search, client, project) => dispatch(Actions.Creators.searchData(search, client, project))
})

// FORM
const form = withFormik({

  mapPropsToValues: () => ({
    search: '',
    client: false,
    project: false
  }),

  // Check errors
  validate: values => {
    const errors = {}

    if (!values.search) {
      errors.search = 'Need to search something'
    }
    if (!values.client && !values.project) {
      errors.project = 'Need at least one filter checked'
    }

    return errors
  },

  // SUBMIT
  handleSubmit: (values, actions) => {
    // Variables
    let search = values.search
    let client = values.client
    let project = values.project

    // Action call
    actions.props.searchData(search, client, project)
    actions.setSubmitting(false)
  }
})

// Export
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  form
)
