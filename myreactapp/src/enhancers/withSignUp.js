// withSignUp.js

// Imports
import { withFormik } from 'formik'
import { connect } from 'react-redux'
import { compose } from 'recompose'

import logActions from '../actions/Actions'

const mapStateToProps = state => (
  {
    ...state.auth
  }
)

const mapDispatchToProps = dispatch => ({
  registerWithEmail: (data) => dispatch(logActions.Creators.registerWithEmail(data))
})

// FORM
const form = withFormik({
  initialValues: {
    user: '',
    email: '',
    password: '',
    confirmPassword: '',
    error: null
  },

  mapPropsToValues: () => ({
    user: '',
    email: '',
    password: '',
    confirmPassword: '',
    error: null
  }),

  // SUBMIT
  handleSubmit: (values, actions) => {
    // Variables
    let password = values.password
    let email = values.email
    const data = {
      password,
      email
    }

    // Action call
    actions.props.registerWithEmail(data)
    actions.setSubmitting(false)
  },

  // Check errors
  validate: (values) => {
    const errors = {}
    if (!values.user) errors.user = 'Enter an username'
    if (!values.password) errors.password = 'Enter a password'
    if (!values.email) errors.email = 'Enter an email'
    if (!values.confirmPassword) errors.confirmPassword = 'Confirm your password'
    if (values.password !== values.confirmPassword) errors.confirmPassword = 'Passwords are different'
    return errors
  }
})

// Export
export default compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  ),
  form
)
