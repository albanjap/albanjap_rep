// index.js
// Imports
import React from 'react'
import { Container, Navbar, Nav } from 'react-bootstrap'
import ReactDOM from 'react-dom'
import { PersistGate } from 'redux-persist/integration/react'
import { Provider } from 'react-redux'
import { BrowserRouter, Route } from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.css'

import LoginPage from './components/View/loginView'
import DashboardPage from './components/View/dashboardView'
import SignUpPage from './components/View/signUpView'

import './index.css'

import rootSagas from './sagas'

import configureStore from './store/configureStore'

const { store, persistor } = configureStore()

rootSagas(store)

ReactDOM.render(
  <div>
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <BrowserRouter>
          <Container className='text-center'>

            {/* Navbar */}
            <Navbar bg='light' expand='lg'>
              <Navbar.Brand href=''>Random website</Navbar.Brand>
              <Navbar.Toggle aria-controls='basic-navbar-nav' />
              <Navbar.Collapse id='basic-navbar-nav'>
                <Nav className='mr-auto'>
                  <Nav.Link href=''>Dasboard</Nav.Link>
                </Nav>
              </Navbar.Collapse>
            </Navbar>

            {/* Routes */}
            <Route exact path='/' component={LoginPage} />
            <Route path='/login' component={LoginPage} />
            <Route path='/dashboard' component={DashboardPage} />
            <Route path='/signUp' component={SignUpPage} />

          </Container>
        </BrowserRouter>
      </PersistGate>
    </Provider>
  </div>
  , document.getElementById('root'))
