// Reducer.js
// Imports
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2'
import { persistReducer } from 'redux-persist'
import { createReducer } from 'reduxsauce'
import storage from 'redux-persist/lib/storage'

import Actions from '../actions/Actions'

// INITIAL_STATE
const INITIAL_STATE = {
  user: null,
  loggedIn: false,
  userData: [],
  loading: false,
  message: null,
  counter: null,
  pathImage: null,
  form: null,
  path: null,
  activeTab: null,
  searchedData: null,
  searchString: null,
  activePage: 1,
  nbPages: null,
  clientPerPage: 5
}

// PERSIST CONFIG
const persistConfig = {
  key: 'root',
  storage,
  stateReconciler: autoMergeLevel2
}

// STATE

const loginSaga = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: false }
}

const logoutSaga = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: false }
}

const registerWithEmailSaga = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: false }
}

const saveUserData = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: false }
}

const resetUserData = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: false }
}

// Success
// A REVOIR
export const loginSuccess = (state = INITIAL_STATE, { userData, counter }) => {
  // Variables
  var listData = []
  var i = 0
  for (let value in userData) {
    if (!isNaN(parseInt(value))) {
      console.log('if for')
      listData[i] = userData[value]
      i++
    }
  }
  console.log('if')
  if (listData.length === 0) {
    listData = null
  }
  console.log(listData)
  const nbPages = (listData.length % state.clientPerPage) === 0 ? listData.length / state.clientPerPage : Math.floor(listData.length / state.clientPerPage) + 1
  console.log(nbPages)

  return { ...state, loggedIn: true, loading: false, message: true, userData: listData, counter, activeTab: 'dashboard', nbPages }
}

export const logoutSuccess = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: false, loggedIn: false, userData: null, counter: null }
}

export const registerWithEmailSuccess = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: false, counter: 0 }
}

// A REVOIR
export const saveUserDataSuccess = (state = INITIAL_STATE, { userData }) => {
  // Variables
  const data = state.userData ? [...state.userData, userData] : [userData]
  const nbPages = (data.length % state.clientPerPage) === 0 ? data.length / state.clientPerPage : Math.floor(data.length / state.clientPerPage) + 1

  return { ...state, loading: false, message: false, userData: data, counter: state.counter + 1, nbPages }
}

// A REVOIR
export const resetUserDataSuccess = (state = INITIAL_STATE, { userData }) => {
  // Variables
  var listData = []
  var i = 0

  // For all the data found
  for (var value in userData) {
    if (!isNaN(parseInt(value))) {
      listData[i] = userData[value]
      i++
    }
  }

  // If they aren't any data
  if (listData.length === 0) {
    listData = null
  }

  const nbPages = (listData.length % state.clientPerPage) === 0
    ? listData.length / state.clientPerPage
    : Math.floor(listData.length / state.clientPerPage) + 1

  return { ...state, loading: false, message: false, userData: listData, pushDatas: false, counter: state.counter - 1, nbPages }
}

// TO DO
export const searchData = (state = INITIAL_STATE, { search, client, project }) => {
  // Variables
  const filterData = state.userData.filter((data) => {
    if (client && project) {
      // If Client and Project are selected
      return data.client.toLowerCase().includes(search.toLowerCase()) ||
        data.project.toLowerCase().includes(search.toLowerCase())
    } else if (client && !project) {
      // If only client is checked
      return data.client.toLowerCase().includes(search.toLowerCase())
    } else if (!client && project) {
      // If only Project is checkd
      return data.project.toLowerCase().includes(search.toLowerCase())
    } else {
      return false
    }
  })

  const nbPages = (filterData.length % state.clientPerPage) === 0
    ? filterData.length / state.clientPerPage
    : Math.floor(filterData.length / state.clientPerPage) + 1

  return { ...state, searchedData: filterData, searchString: search, nbPages, activePage: 1 }
}

// Failures
export const loginFailure = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: 'login Failure' }
}

export const registerWithEmailFailure = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: 'register Failure' }
}

export const logoutFailure = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: 'logout Failure' }
}

export const saveUserDataFailure = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: 'data failure', userData: false }
}

export const resetUserDataFailure = (state = INITIAL_STATE) => {
  return { ...state, loading: false, message: 'reset data failure', userData: true }
}

export const selectedTab = (state = INITIAL_STATE, { key }) => {
  return { ...state, loading: false, form: key }
}

export const changePage = (state = INITIAL_STATE, { page }) => {
  return { ...state, activePage: parseInt(page) }
}

const Types = Actions.Types

// HANDLERS
const HANDLERS = createReducer(INITIAL_STATE, {
  // Login
  [Types.LOGIN]: loginSaga,
  [Types.LOGIN_SUCCESS]: loginSuccess,
  [Types.LOGIN_FAILURE]: loginFailure,

  // Logout
  [Types.LOGOUT]: logoutSaga,
  [Types.LOGOUT_SUCCESS]: logoutSuccess,
  [Types.LOGOUT_FAILURE]: logoutFailure,

  // Register
  [Types.REGISTER_WITH_EMAIL]: registerWithEmailSaga,
  [Types.REGISTER_WITH_EMAIL_SUCCESS]: registerWithEmailSuccess,
  [Types.REGISTER_WITH_EMAIL_FAILURE]: registerWithEmailFailure,

  // Save datas
  [Types.SAVE_USER_DATA]: saveUserData,
  [Types.SAVE_USER_DATA_SUCCESS]: saveUserDataSuccess,
  [Types.SAVE_USER_DATA_FAILURE]: saveUserDataFailure,

  // Reset datas
  [Types.RESET_USER_DATA]: resetUserData,
  [Types.RESET_USER_DATA_SUCCESS]: resetUserDataSuccess,
  [Types.RESET_USER_DATA_FAILURE]: resetUserDataFailure,

  // Selected Tab
  [Types.SELECTED_TAB]: selectedTab,

  // Search Data
  [Types.SEARCH_DATA]: searchData,

  // Change Page
  [Types.CHANGE_PAGE]: changePage

})

const persistedReducer = persistReducer(persistConfig, HANDLERS)

// Export
export default persistedReducer
