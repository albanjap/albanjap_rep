// index.js from Reducers
// Imports
import 'bootstrap/dist/css/bootstrap.css'
import { combineReducers } from 'redux'

import authReducer from './Reducer'

const rootReducer = combineReducers({
  auth: authReducer
})

// Export
export default rootReducer // makes them available
