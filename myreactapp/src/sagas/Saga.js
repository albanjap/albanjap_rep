// Saga.js
// Imports
import firebase from 'firebase'
import rsf from '../firebase/rsf'
import { call, fork, put, takeEvery, all } from 'redux-saga/effects'

import logActions from '../actions/Actions'

// Login
function * loginSaga (payload) {
  try {
    // Variables
    var response = yield call(rsf.auth.signInWithEmailAndPassword, payload.data.email, payload.data.password)
    var { user } = response
    var data = yield call(rsf.firestore.getDocument, 'user/' + user.uid)
    var userData = []
    userData = data.data().counter === 0 ? null : { ...data.data(), counter: undefined, i: undefined }

    yield put(logActions.Creators.loginSuccess(userData, data.data().counter))
  } catch (error) {
    console.log('error login Saga')
    console.log(error)

    yield put(logActions.Creators.loginFailure(error))
  }
}

// Register
function * registerWithEmailSaga (payload) {
  try {
    // Variables
    const data = yield call(rsf.auth.createUserWithEmailAndPassword, payload.data.email, payload.data.password)
    var uid = data.user.uid

    // Create the document with the fields
    firebase.firestore().collection('user').doc(uid).set(
      {
        email: data.user.email,
        displayName: data.user.displayName,
        counter: 0,
        i: 0
      }
    )

    yield put(logActions.Creators.registerWithEmailSuccess(data))
  } catch (error) {
    console.log('error register Saga')
    console.log(error)

    yield put(logActions.Creators.registerWithEmailFailure(error))
  }
}

// Logout
function * logoutSaga () {
  try {
    // Variables
    const data = yield call(rsf.auth.signOut)

    yield put(logActions.Creators.logoutSuccess(data))
  } catch (error) {
    console.log('error logout Saga')
    console.log(error)

    yield put(logActions.Creators.logoutFailure(error))
  }
}

// Save Data
function * saveUserData ({ userData }) {
  try {
    // Variables
    var user = firebase.auth().currentUser

    if (user != null) {
      var uid = user.uid
    }

    var image = userData.image
    var metadata = {
      contentType: 'image/jpeg'
    }

    const data = yield call(rsf.database.read, 'user/' + uid)

    // If there is an image
    if (image) {
      yield firebase.storage().ref(uid + '/' + data.i).put(image, metadata)
    }

    // If there is an image
    if (image) {
      // Get the image
      var path = yield firebase.storage().ref(uid + '/' + data.i).getDownloadURL()
    } else {
      path = null
    }

    // Update the document
    yield firebase.firestore().collection('user').doc(uid).update({ [data.i]: { ...userData, id: data.i, image: path } })
    yield firebase.firestore().collection('user').doc(uid).update({ counter: data.counter + 1, i: data.i + 1 })

    yield put(logActions.Creators.saveUserDataSuccess({ ...userData, id: data.i, image: path }))
  } catch (error) {
    console.log(error)
    console.log('error in saving data')

    yield put(logActions.Creators.saveUserDataFailure(error))
  }
}

// Reset Data
function * resetUserData ({ payload }) {
  try {
    // Variables
    var user = firebase.auth().currentUser
    if (user != null) {
      var uid = user.uid
    }
    var data = yield call(rsf.database.read, 'user/' + uid)

    // If there is an image
    if (data.image) {
      // Delete it
      yield firebase.storage().ref(uid + '/' + payload).delete()
    }

    // Update the document
    yield firebase.firestore().collection('user').doc(uid).update({ [payload]: null })
    yield firebase.firestore().collection('user').doc(uid).update({ counter: data.counter - 1 })

    const userData = yield call(rsf.database.read, 'user/' + uid)
    const response = { ...userData, i: undefined, counter: undefined }

    yield put(logActions.Creators.resetUserDataSuccess(response))
  } catch (error) {
    console.log(error)
    console.log('error in resetting data')

    yield put(logActions.Creators.resetUserDataFailure(error))
  }
}

// filter for searching
// var first = db.collection('user')
//   .orderBy('id')
//   .limit(3)

// var paginate = first.get()
//   .then((snapshot) => {
//     var last = snapshot.docs[snapshot.docs.length - 1];

//     var next = db.collection('user')
//       .orderBy('id')
//       .startAfter(last.data().id)
//       .limit(3)

//   })

// WATCH COUNTER
const watchCounter = () =>
  function * watch () {
    yield takeEvery(logActions.Types.LOGIN, loginSaga)
    yield takeEvery(logActions.Types.REGISTER_WITH_EMAIL, registerWithEmailSaga)
    yield takeEvery(logActions.Types.LOGOUT, logoutSaga)
    yield takeEvery(logActions.Types.SAVE_USER_DATA, saveUserData)
    yield takeEvery(logActions.Types.RESET_USER_DATA, resetUserData)
  }

// Export
export default function * RootSaga () {
  yield all([fork(watchCounter())])
}
