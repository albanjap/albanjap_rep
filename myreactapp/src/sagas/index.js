// index.js from sagas
// Import
import RootSaga from './Saga'

const rootSagas = store => {
  store.runSaga(RootSaga)
}

// Export
export default rootSagas
