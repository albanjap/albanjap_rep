// configureStore.js
// Imports
import { createStore, applyMiddleware } from 'redux'
import { persistStore } from 'redux-persist'
import createSagaMiddleware from 'redux-saga'

import rootReducer from '../reducers'

// CONFIGURE STORE
const configureStore = () => {
  // Create Saga
  const sagaMiddleware = createSagaMiddleware()

  // Create the Store
  const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
  const persistor = persistStore(store)
  store.runSaga = sagaMiddleware.run

  return { store, persistor }
}

// Export
export default configureStore
